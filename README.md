
# Real estate

Backend with business logic for ProPart app



## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/Denis307wfs/real-estate2.git
```

Go to the develop branch

```bash
  git checkout develop
```

Build project

```bash
  ./gradlew build
```

Run docker file

```bash
  sudo docker build -t real-estate-app .
```

Start the server

```bash
  sudo docker-compose --env-file ./src/main/resources/.env up -d
```

