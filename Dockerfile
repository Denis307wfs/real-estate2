FROM gradle:latest AS builder
COPY . /home/gradle
WORKDIR /home/gradle
RUN gradle clean build --no-daemon

FROM openjdk:17-oracle
COPY --from=builder /home/gradle/build/libs/*.jar /home/application.jar
EXPOSE ${CONTAINER_PORT}
ENTRYPOINT ["java", "-jar", "/home/application.jar"]
