package com.realestate.team2.enums;

public enum Rooms {
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    STUDIO
}
