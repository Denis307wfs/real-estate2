package com.realestate.team2.enums;

public enum PropertyType {
    PLOT, TOWNHOUSE, APARTMENT, VILLA
}
