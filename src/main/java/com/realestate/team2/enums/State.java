package com.realestate.team2.enums;

public enum State {
    STEP_ONE, STEP_TWO, STEP_THREE, DONE
}
