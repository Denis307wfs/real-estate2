package com.realestate.team2.enums;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants(onlyExplicitlyIncluded = true)
@RequiredArgsConstructor
public enum Role {
    ROLE_USER(Constants.USER_VALUE),
    ROLE_BROKER(Constants.BROKER_VALUE),
    ROLE_CLIENT(Constants.CLIENT_VALUE),
    ROLE_MODERATOR(Constants.MODERATOR_VALUE),
    ROLE_ADMIN(Constants.ADMIN_VALUE);

    public static class Constants {
        public static final String USER_VALUE = "ROLE_USER";
        public static final String BROKER_VALUE = "ROLE_BROKER";
        public static final String CLIENT_VALUE = "ROLE_CLIENT";
        public static final String MODERATOR_VALUE = "ROLE_MODERATOR";
        public static final String ADMIN_VALUE = "ROLE_ADMIN";
    }

    private final String name;
}
