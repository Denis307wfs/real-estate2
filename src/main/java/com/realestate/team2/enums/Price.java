package com.realestate.team2.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum Price {
    ZERO(0),
    ONE_FIFTY_K(150_000),
    TWO_FIFTY_K(250_000),
    FIVE_HUNDRED_K(500_000),
    ONE_MILLION(1_000_000),
    FIVE_MILLION(5_000_000),
    UNLIMITED(2_100_000_000);

    private final int value;

    public static Price fromInt(int value) {
        return Arrays.stream(values())
                .filter(price -> price.value == value)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No enum constant with value: " + value));
    }
}
