package com.realestate.team2.enums;

public enum Facility {
    GYM,
    BANK,
    CONCIERGE,
    ELEVATOR,
    STORAGE_ROOM,
    GREEN_AREAS,
    PRIVATE_POOL,
    SEA_VIEW,
    AIR_CONDITIONING,
    PRIVATE_PARKING,
    PRIVATE_ENTRANCE,
    BUILT_IN_WARDROBES,
    GARAGE,
    SAUNA,
    CINEMA
}
