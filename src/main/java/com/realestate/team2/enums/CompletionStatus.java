package com.realestate.team2.enums;

public enum CompletionStatus {
    OFF_PLAN,
    SECONDARY,
    READY,
    SPECIAL_OFFER
}
