package com.realestate.team2.exception.handler;

import com.realestate.team2.exception.ApiError;
import com.realestate.team2.exception.exceptions.ApplicationException;
import jakarta.servlet.ServletException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({ApplicationException.class})
    public ResponseEntity<Object> handleApplicationException(final ApplicationException ex) {
        log.error("handleApplicationException", ex);
        return createApiError(ex.getStatus(), ex.getLocalizedMessage());
    }

    @ExceptionHandler({Exception.class, ServletException.class})
    public ResponseEntity<?> handleException(Exception ex) {
        log.error(ex.getMessage());
        return createApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage());
    }

    @ExceptionHandler({AuthenticationException.class,})
    @ResponseBody
    public ResponseEntity<?> handleAuthenticationException(Exception ex) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleValidException(MethodArgumentNotValidException ex) {

        return createApiError(
                HttpStatus.BAD_REQUEST,
                """
                Error not valid data!
                minSize, maxSize, minPrice, maxPrice must be positive or zero.
                """
                );
    }

    private ResponseEntity<Object> createApiError(HttpStatus status, String details) {
        final ApiError apiError = new ApiError(details, details, status);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }
}
