package com.realestate.team2.exception.exceptions;

public class SaveUserException extends Exception {
    public SaveUserException() {
        super("User with this phone already exists");
    }
}
