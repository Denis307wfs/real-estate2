package com.realestate.team2.service;

import com.realestate.team2.dto.project.ProjectDto;
import com.realestate.team2.entity.ProjectEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProjectService {

    void createProject(ProjectDto dto);

    public void deleteProject(String projectId);

    public void setHideOption(String projectId, Boolean isHidden);

    ProjectDto getProjectById(String projectId);

    Page<ProjectDto> getBrokersProjects(Pageable pageable);

    void updateProject(String projectId, ProjectDto dto);

    ProjectEntity getProjectEntityById(String projectId);

}
