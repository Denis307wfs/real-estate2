package com.realestate.team2.service;

import com.realestate.team2.dto.project.SavedUserProjectDto;
import org.bson.types.ObjectId;

public interface SavedUserProjectService {
    SavedUserProjectDto getSavedUserProjects();

    void addToSavedUserProjects(ObjectId projectId);

    void removeFromSavedUserProjects(ObjectId projectId);
}
