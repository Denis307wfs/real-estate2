package com.realestate.team2.service;

import com.realestate.team2.dto.user.RequestLoginDto;
import com.realestate.team2.dto.user.ResponseLoginDto;
import jakarta.servlet.http.HttpServletRequest;

public interface AuthenticationService {

    ResponseLoginDto login(RequestLoginDto requestLoginDto);

    ResponseLoginDto refreshToken(HttpServletRequest httpServletRequest);
}
