package com.realestate.team2.service;

import com.realestate.team2.model.ProjectFilter;

public interface NotificationService {

    void notifyAllInterestedUsersInProject(ProjectFilter projectFilter);
}
