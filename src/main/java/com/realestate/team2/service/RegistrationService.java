package com.realestate.team2.service;

import com.realestate.team2.dto.token.TokenDto;
import com.realestate.team2.dto.user.RequestSaveUserDto;
import com.realestate.team2.dto.user.ResponseLoginDto;
import com.realestate.team2.dto.user.RoleRegisterDto;

public interface RegistrationService {

    void registration(RequestSaveUserDto dto);

    ResponseLoginDto setRole(RoleRegisterDto roleRegisterDto, TokenDto tokenDto);
}
