package com.realestate.team2.service.impl;

import com.realestate.team2.dto.token.TokenDto;
import com.realestate.team2.dto.user.RequestLoginDto;
import com.realestate.team2.dto.user.ResponseLoginDto;
import com.realestate.team2.dto.user.UserDto;
import com.realestate.team2.service.AuthenticationService;
import com.realestate.team2.service.UserService;
import com.realestate.team2.service.token.TokenBuildService;
import com.realestate.team2.service.token.TokenExtractClaimsService;
import com.realestate.team2.service.token.TokenService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final AuthenticationManager authenticationManager;

    private final TokenBuildService tokenBuildService;

    private final TokenExtractClaimsService tokenExtractClaimsService;

    private final TokenService tokenService;

    private final UserService userService;

    @Override
    public ResponseLoginDto login(RequestLoginDto requestLoginDto) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                requestLoginDto.phoneNumber(), requestLoginDto.sms()
        ));
        authentication.getDetails();

        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDto userDto = userService.getUserByPhoneNumber(requestLoginDto.phoneNumber());

        String refreshToken = tokenBuildService.generateRefreshToken(userDto);
        TokenDto refreshTokenDto = tokenService.saveRefreshToken(refreshToken);

        String accessToken = tokenBuildService.generateAccessToken(userDto, refreshTokenDto.getId());
        return new ResponseLoginDto(accessToken, userDto.getState().toString(), userDto.getRole().toString());
    }

    @Override
    public ResponseLoginDto refreshToken(HttpServletRequest httpServletRequest) {
        TokenDto tokenDto = tokenService.getAccessToken(httpServletRequest);
        ObjectId userId = tokenExtractClaimsService.extractUserId(tokenDto.getToken());
        UserDto userDto = userService.getUserById(userId);
        boolean isAccessTokenValid = tokenExtractClaimsService.isTokenValid(tokenDto.getToken());
        if (isAccessTokenValid) {
            return new ResponseLoginDto(tokenDto.getToken(),
                    userDto.getState().toString(), userDto.getRole().toString());
        }
        ObjectId refreshTokenId =
                tokenExtractClaimsService.extractRefreshTokenId(tokenDto.getToken());
        TokenDto refreshTokenDto = tokenService.getRefreshToken(refreshTokenId);
        if (refreshTokenDto == null) {
            throw new RuntimeException("User is not authenticated.");
        }
        boolean isRefreshTokenValid = tokenExtractClaimsService.isTokenValid(refreshTokenDto.getToken());

        if (isRefreshTokenValid) {
            String accessToken = tokenBuildService.generateAccessToken(userDto, refreshTokenDto.getId());
            return new ResponseLoginDto(accessToken, userDto.getState().toString(), userDto.getRole().toString());
        } else {
            tokenService.removeRefreshToken(refreshTokenId);
            throw new RuntimeException("User has to pass authentication. Refresh token is expired.");
        }
    }

}
