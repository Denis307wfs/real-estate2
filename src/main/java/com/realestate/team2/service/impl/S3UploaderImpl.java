package com.realestate.team2.service.impl;

import com.realestate.team2.service.S3Uploader;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class S3UploaderImpl implements S3Uploader {

    @Override
    public String uploadAndReturnUrl(MultipartFile multipartFile) {
        return "url";
    }
}
