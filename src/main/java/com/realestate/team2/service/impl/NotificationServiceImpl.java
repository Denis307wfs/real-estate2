package com.realestate.team2.service.impl;

import com.realestate.team2.model.ProjectFilter;
import com.realestate.team2.service.ClientService;
import com.realestate.team2.service.NotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class NotificationServiceImpl implements NotificationService {
    private static final String CREATE_PROJECT_TITLE = "A new project has been added";
    private static final String CREATE_PROJECT_MESSAGE = "A new project was added with the following properties: ";
    private final ExpoService expoService;
    private final ClientService clientService;

    @Override
    public void notifyAllInterestedUsersInProject(ProjectFilter projectFilter) {
        List<List<String>> expoTokens = clientService.getAllExpoTokensForProjectFilter(projectFilter);
        if (expoTokens.isEmpty() || expoTokens.stream().allMatch(Objects::isNull)) {
            return;
        }
        Map<String, Object> data = addData(projectFilter);
        for (List<String> tokens : expoTokens) {
            tokens.forEach(token ->
                    expoService.sendPushNotification(token, CREATE_PROJECT_TITLE, CREATE_PROJECT_MESSAGE, data));
        }
    }

    private Map<String, Object> addData(ProjectFilter projectFilter) {
        Map<String, Object> data = new HashMap<>();
        data.put("Property Type", projectFilter.getPropertyType());
        data.put("Price", projectFilter.getPrice());
        data.put("Rooms", projectFilter.getRooms());
        return data;
    }
}
