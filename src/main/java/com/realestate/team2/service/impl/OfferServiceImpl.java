package com.realestate.team2.service.impl;

import com.realestate.team2.dto.filter.DynamicFilterDto;
import com.realestate.team2.dto.project.ProjectDto;
import com.realestate.team2.entity.ClientEntity;
import com.realestate.team2.entity.ProjectEntity;
import com.realestate.team2.entity.UserEntity;
import com.realestate.team2.enums.Area;
import com.realestate.team2.enums.PropertyType;
import com.realestate.team2.enums.Rooms;
import com.realestate.team2.exception.exceptions.ApplicationException;
import com.realestate.team2.maper.ProjectMapper;
import com.realestate.team2.model.ClientFilter;
import com.realestate.team2.repository.ClientRepository;
import com.realestate.team2.repository.ProjectRepository;
import com.realestate.team2.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OfferServiceImpl implements OfferService {

    private static String COLLECTION_NAME = "generalInfo";

    @Autowired
    private ProjectMapper projectMapper;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Page<ProjectDto> getOffersByUserId(Pageable pageable) {
        UserEntity userEntity = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userId = userEntity.getId().toString();

        return this.getOffers(userId, pageable);
    }

    @Override
    public Page<ProjectDto> getOffers(Pageable pageable) {
        return projectRepository.findAllCustom(pageable);
    }

    // Task 11. display of offers for me based on initial registration - 4p.
    @Override
    public Page<ProjectDto> getOffers(String userId, Pageable pageable) throws ApplicationException {
        ClientEntity clientEntity = clientRepository.findByUserId(userId).orElseThrow(
                () -> new ApplicationException(
                        HttpStatus.NOT_FOUND,
                        """
                            Error when i try to get ClientFilter!
                        User, are not exist!
                        """
                )
        );

        ClientFilter clientFilter = clientEntity.getClientFilter();

        if (isClientFilterBroken(clientFilter)) {
            throw new ApplicationException(
                    HttpStatus.UNPROCESSABLE_ENTITY,
                    "Error ClientFilter is broken!"
            );
        }

        return projectRepository.findByClientFilter(
                pageable,
                clientFilter.getPropertyTypes(),
                clientFilter.getMinPrice(),
                clientFilter.getMaxPrice(),
                clientFilter.getRooms()
        );
    }

    // Task 12. filtering projects on the back end for the user - 4p.
    @Override
    public Page<ProjectDto> getOffers(
            DynamicFilterDto dynamicFilterDto, Pageable pageable
    ) throws ApplicationException {

        // Set up query criteria
        Criteria criteria = new Criteria();
        this.addEnumCriteria(
                criteria,
                dynamicFilterDto.area(),
                Area.class,
                "area"
        );
        this.addEnumCriteria(
                criteria,
                dynamicFilterDto.propertyTypes(),
                PropertyType.class,
                "propertyType"
        );
        this.addRangeCriteria(
                criteria,
                dynamicFilterDto.minSize(),
                dynamicFilterDto.maxSize(),
                "size"
        );
        this.addEnumCriteria(
                criteria,
                dynamicFilterDto.rooms(),
                Rooms.class,
                "rooms"
        );
        this.addRangeCriteria(
                criteria,
                dynamicFilterDto.minPrice(),
                dynamicFilterDto.maxPrice(),
                "price"
        );

        // Get Entity list
        Query query = new Query(criteria).with(pageable);
        List<ProjectEntity> projectEntities = mongoTemplate.find(
                query,
                ProjectEntity.class
        );

        // Convert Entity to DTO
        List<ProjectDto> projectDtoList = projectEntities.stream()
                .map(projectMapper::toDto)
                .collect(Collectors.toList());

        // Convert List to Page
        Query queryNoPage = new Query(criteria);
        return PageableExecutionUtils.getPage(
                projectDtoList,
                pageable,
                () -> mongoTemplate.count(queryNoPage, ProjectEntity.class)
        );
    }

    private boolean isClientFilterBroken(ClientFilter clientFilter) {
        if (clientFilter == null) {
            return true;
        }

        return clientFilter.getPropertyTypes() == null
                || clientFilter.getMinPrice() == null
                || clientFilter.getMaxPrice() == null
                || clientFilter.getRooms() == null;
    }

    private void addRangeCriteria(Criteria criteria, Integer minBorder, Integer maxBorder, String columnName) {
        if (minBorder != null && maxBorder != null) {
            if (minBorder > maxBorder) {
                throw new ApplicationException(
                        HttpStatus.UNPROCESSABLE_ENTITY,
                        String.format(
                            """
                                Data validation error!
                            min %s > max %s.
                            You must have mixed up min and max number.
                            I will not process this request to avoid confusion.
                            """,
                            columnName, columnName
                        )
                );
            } else {
                criteria.and(COLLECTION_NAME + "." + columnName)
                        .gte(minBorder)
                        .lte(minBorder);
            }
        } else if (minBorder != null) {
            criteria.and(COLLECTION_NAME + "." + columnName)
                    .gte(maxBorder);
        } else if (maxBorder != null) {
            criteria.and(COLLECTION_NAME + "." + columnName)
                    .lte(minBorder);
        }
    }

    private <T extends Enum<T>> void addEnumCriteria(
            Criteria criteria, List<String> emunStringList, Class<T> enumClass, String columnName
    ) throws ApplicationException {
        if (emunStringList != null) {
            if (!emunStringList.isEmpty()) {
                for (String s : emunStringList) {
                    try {
                        Enum.valueOf(enumClass, s);

                    } catch (IllegalArgumentException e) {
                        throw new ApplicationException(
                                HttpStatus.UNPROCESSABLE_ENTITY,
                                "Error! Not valid string for enum" + enumClass.getTypeName()
                        );
                    }
                }
                criteria.and(COLLECTION_NAME + "." + columnName).in(
                        emunStringList
                );
            }
        }
    }

    private <T extends Enum<T>> void addEnumCriteria(
            Criteria criteria, String emunString, Class<T> enumClass, String columnName
    ) throws ApplicationException {
        if (emunString != null) {
            if (!emunString.isEmpty()) {
                try {
                    Enum.valueOf(enumClass, emunString);

                } catch (IllegalArgumentException e) {
                    throw new ApplicationException(
                            HttpStatus.UNPROCESSABLE_ENTITY,
                            "Error! Not valid string for enum" + enumClass.getTypeName()
                    );
                }
                criteria.and(COLLECTION_NAME + "." + columnName).is(
                        emunString
                );
            }
        }
    }

}
