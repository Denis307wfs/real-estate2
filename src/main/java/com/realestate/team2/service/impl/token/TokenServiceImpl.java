package com.realestate.team2.service.impl.token;

import com.realestate.team2.dto.token.TokenDto;
import com.realestate.team2.entity.DisabledAccessTokenEntity;
import com.realestate.team2.entity.TokenEntity;
import com.realestate.team2.maper.TokenMapper;
import com.realestate.team2.repository.DisabledAccessTokenRepository;
import com.realestate.team2.repository.TokenRepository;
import com.realestate.team2.service.token.TokenService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.bson.types.ObjectId;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Service
public class TokenServiceImpl implements TokenService {

    private static final String TOKEN_PREFIX = "Bearer ";

    private final TokenMapper tokenMapper;

    private final TokenRepository tokenRepository;

    private final DisabledAccessTokenRepository disabledAccessTokenRepository;

    @Override
    @SneakyThrows
    public TokenDto getAccessToken(HttpServletRequest request) {
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (header != null && header.startsWith(TOKEN_PREFIX)) {
            return TokenDto.builder().token(header.substring(7)).build();
        }
        return null;
    }

    @Override
    public TokenDto getRefreshToken(ObjectId id) {
        var token = tokenRepository.findById(id);
        return token.map(tokenMapper::toTokenDto).orElse(null);
    }

    @Override
    public TokenDto getDisabledAccessToken(String disabledAccessToken) {
        var token = disabledAccessTokenRepository.findByDisabledAccessToken(disabledAccessToken);
        return token.map(tokenMapper::toTokenDto).orElse(null);
    }

    @Override
    public TokenDto saveRefreshToken(String refreshToken) {
        LocalDateTime now = LocalDateTime.now();
        return tokenMapper.toTokenDto(tokenRepository
                .save(TokenEntity.builder()
                        .refreshToken(refreshToken)
                        .createdAt(now)
                        .updatedAt(now).build()));
    }

    @Override
    public TokenDto saveDisabledAccessToken(String disabledAccessToken) {
        LocalDateTime now = LocalDateTime.now();
        return tokenMapper.toTokenDto(disabledAccessTokenRepository
                .save(DisabledAccessTokenEntity.builder()
                        .disabledAccessToken(disabledAccessToken)
                        .createdAt(now)
                        .updatedAt(now).build()));
    }

    @Override
    public void removeRefreshToken(ObjectId id) {
        tokenRepository.deleteById(id);
    }
}
