package com.realestate.team2.service.impl;

import com.realestate.team2.dto.project.SavedUserProjectDto;
import com.realestate.team2.entity.SavedUserProjectEntity;
import com.realestate.team2.entity.UserEntity;
import com.realestate.team2.maper.SavedUserProjectMapper;
import com.realestate.team2.repository.SavedUserProjectRepository;
import com.realestate.team2.service.SavedUserProjectService;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SavedUserProjectServiceImpl implements SavedUserProjectService {

    private final SavedUserProjectRepository savedUserProjectRepository;

    private final SavedUserProjectMapper savedUserProjectMapper;

    @Override
    public SavedUserProjectDto getSavedUserProjects() {
        String userId = this.getAuthenticatedUserId();
        return savedUserProjectMapper.toSavedUserProjectDto(
                savedUserProjectRepository
                        .findSavedUserProjectEntityByUserId(new ObjectId(userId)));
    }

    @Override
    public void addToSavedUserProjects(ObjectId projectId) {
        String userId = this.getAuthenticatedUserId();
        SavedUserProjectEntity savedUserProjectEntity = savedUserProjectRepository
                .findSavedUserProjectEntityByUserId(new ObjectId(userId));
        savedUserProjectEntity.getSavedProjectIds().add(projectId);
        savedUserProjectRepository.save(savedUserProjectEntity);
    }

    @Override
    public void removeFromSavedUserProjects(ObjectId projectId) {
        String userId = this.getAuthenticatedUserId();
        SavedUserProjectEntity savedUserProjectEntity = savedUserProjectRepository
                .findSavedUserProjectEntityByUserId(new ObjectId(userId));
        savedUserProjectEntity.getSavedProjectIds().remove(projectId);
        savedUserProjectRepository.save(savedUserProjectEntity);
    }

    private String getAuthenticatedUserId() {
        UserEntity userEntity = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userEntity.getId().toString();
    }
}
