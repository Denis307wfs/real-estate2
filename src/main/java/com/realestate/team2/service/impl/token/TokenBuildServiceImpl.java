package com.realestate.team2.service.impl.token;

import com.realestate.team2.dto.user.UserDto;
import com.realestate.team2.service.token.TokenBuildService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class TokenBuildServiceImpl implements TokenBuildService {

    private final Key key;

    @Value("${security.access-token.expiration}")
    private long accessExpiration;

    @Value("${security.refresh-token.expiration}")
    private long refreshExpiration;

    @Override
    public String generateAccessToken(UserDto userDto, ObjectId refreshTokenId) {
        return buildToken(Map.of("role", userDto.getRole().name(),
                "user_id", userDto.getId().toHexString(),
                "refresh_token_id", refreshTokenId.toHexString()), userDto, accessExpiration);
    }

    @Override
    public String generateRefreshToken(UserDto userDto) {
        return buildToken(new HashMap<>(), userDto, refreshExpiration);
    }

    private String buildToken(Map<String, Object> extraClaims,
                              UserDto userDto,
                              long expiration) {

        Date currentTime = new Date(System.currentTimeMillis());
        return Jwts.builder()
                .setClaims(extraClaims)
                .setSubject(userDto.getPhoneNumber())
                .setIssuedAt(currentTime)
                .setExpiration(new Date(currentTime.getTime() + expiration))
                .signWith(key, SignatureAlgorithm.HS256)
                .compact();
    }
}
