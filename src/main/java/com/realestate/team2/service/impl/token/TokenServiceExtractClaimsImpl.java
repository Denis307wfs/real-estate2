package com.realestate.team2.service.impl.token;

import com.realestate.team2.entity.UserEntity;
import com.realestate.team2.enums.Role;
import com.realestate.team2.service.token.TokenExtractClaimsService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Collection;
import java.util.Date;
import java.util.function.Function;

@RequiredArgsConstructor
@Service
public class TokenServiceExtractClaimsImpl implements TokenExtractClaimsService {

    private final Key key;

    @Override
    public String extractPhoneNumber(String token) {
        return extractClaims(token, Claims::getSubject);
    }

    @Override
    public String extractRole(String token) {
        return extractClaims(token,
                (claims) -> claims.get("role", String.class));
    }

    @Override
    public ObjectId extractUserId(String token) {
        return new ObjectId((String) extractClaims(token,
                (claims) -> claims.get("user_id", String.class)));
    }

    @Override
    public ObjectId extractRefreshTokenId(String token) {
        return new ObjectId((String) extractClaims(token,
                (claims) -> claims.get("refresh_token_id", String.class)));
    }

    @Override
    public Date extractExpirationDate(String token) {
        return extractClaims(token, Claims::getExpiration);
    }

    @Override
    public boolean isTokenValid(String token) {
        try {
            Jwts.parserBuilder()
                    .setSigningKey(key).build().parse(token);
        } catch (Exception e) {
            return false;
        }
        return isTokenNonExpired(token);
    }

    @Override
    public Authentication getAuthentication(String token) {
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();

        UserEntity principal = UserEntity.builder().build();
        principal.setId(new ObjectId(String.valueOf(claims.get("user_id"))));
        principal.setPhoneNumber(claims.getSubject());
        principal.setRole(Role.valueOf((String)claims.get("role")));

        Collection<? extends GrantedAuthority> authorities = principal.getAuthorities();

        return new UsernamePasswordAuthenticationToken(principal, token, authorities);
    }

    private boolean isTokenNonExpired(String token) {
        Date date = extractClaims(token, Claims::getExpiration);
        return date.after(new Date());
    }

    private <T> T extractClaims(String token, Function<Claims, T> claimsResolver) {
        Claims claims;
        try {
            claims = extractAllClaims(token);
        } catch (ExpiredJwtException e) {
            claims = e.getClaims();
        }
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }
}
