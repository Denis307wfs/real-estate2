package com.realestate.team2.service.impl;

import com.realestate.team2.dto.token.TokenDto;
import com.realestate.team2.dto.user.*;
import com.realestate.team2.entity.UserEntity;
import com.realestate.team2.enums.Role;
import com.realestate.team2.enums.State;
import com.realestate.team2.exception.exceptions.ApplicationException;
import com.realestate.team2.maper.UserMapper;
import com.realestate.team2.repository.UserRepository;
import com.realestate.team2.service.UserService;
import com.realestate.team2.service.token.TokenBuildService;
import com.realestate.team2.service.token.TokenExtractClaimsService;
import com.realestate.team2.service.token.TokenService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final TokenBuildService tokenBuildService;
    private final TokenService tokenService;
    private final TokenExtractClaimsService tokenExtractClaimsService;

    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        return userRepository.findByPhoneNumber(phoneNumber)
                .orElseThrow(() -> new ApplicationException(
                        HttpStatus.BAD_REQUEST,
                        "User with phoneNumber %s doesn't exist".formatted(phoneNumber)));
    }

    @Override
    @SneakyThrows
    public UserDto getUserById(ObjectId userId) {
        return userMapper.toUserDto(userRepository.findById(userId)
                .orElseThrow(RuntimeException::new));
    }

    @Override
    @SneakyThrows
    public UserDto getUserByPhoneNumber(String phoneNumber) {
        return userMapper.toUserDto(userRepository.findByPhoneNumber(phoneNumber)
                .orElseThrow(RuntimeException::new));
    }

    @Override
    public String getStateByUserId(ObjectId userId) {
        Document document = userRepository.findStateByUserId(userId).orElseThrow(() ->
                new ApplicationException(HttpStatus.BAD_REQUEST,
                        "Can not find state for user with id %s".formatted(userId.toString())));
        return (String) document.get("state");
    }

    @Override
    @SneakyThrows
    public UserDto saveUserWithPhone(RequestSaveUserDto requestSaveUserDto) {
        UserEntity userEntity = userRepository.findByPhoneNumber(requestSaveUserDto.phoneNumber()).orElse(null);

        if (userEntity != null) {
            log.warn("User with phone number {} is already exist", requestSaveUserDto.phoneNumber());
        } else {
            userEntity = userMapper.toUserEntity(requestSaveUserDto);
            userEntity.setRole(Role.ROLE_USER);
            userEntity.setState(State.STEP_ONE);
            userEntity.setCreatedAt(LocalDateTime.now());
            userEntity.setUpdatedAt(userEntity.getCreatedAt());
            userEntity.setEnabled(true);
            userEntity.setLocked(false);
            userEntity.setExpoTokens(Collections.emptyList());
        }
        //todo: send OTP password to user and set it as a password param
        userEntity.setPassword(passwordEncoder.encode("1234"));

        return userMapper.toUserDto(userRepository.save(userEntity));
    }

    @Override
    @SneakyThrows
    public void updateUser(RequestUpdateUserDto requestUpdateUserDto) {
        try {
            UserEntity user = userRepository.findById(requestUpdateUserDto.id())
                    .orElseThrow(RuntimeException::new);
            user.setRole(Role.valueOf(requestUpdateUserDto.role()));
            user.setUpdatedAt(LocalDateTime.now());
            userRepository.save(user);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public void updateStateById(ObjectId id, State newState) {
        UserEntity user = userRepository.findById(id)
                .orElseThrow(() -> new ApplicationException(HttpStatus.BAD_REQUEST,
                        "User with id %s was not found".formatted(id)));
        user.setState(newState);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(ObjectId userId) {
        userRepository.deleteById(userId);
    }

    @Override
    @SneakyThrows
    public ResponseLoginDto setRole(RoleRegisterDto dto, TokenDto tokenDto) {
        UserEntity userEntity = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDto userDto = getUserByPhoneNumber(userEntity.getPhoneNumber());
        String userRole = "ROLE_" + dto.role().toUpperCase();
        try {
            userDto.setRole(Role.valueOf(userRole));
            userRepository.save(userMapper.toUserEntity(userDto));
        } catch (IllegalArgumentException ex) {
            log.warn("Incorrect role '{}'", dto.role());
            throw new ApplicationException(HttpStatus.BAD_REQUEST,
                    "Incorrect role '%s'".formatted(dto.role().toUpperCase()));
        }
        tokenService.saveDisabledAccessToken(tokenDto.getToken());
        ObjectId refreshTokenId = tokenExtractClaimsService.extractRefreshTokenId(tokenDto.getToken());
        String accessToken = tokenBuildService.generateAccessToken(userDto, refreshTokenId);

        return new ResponseLoginDto(accessToken, userDto.getRole().toString(), userDto.getState().toString());
    }

    @Override
    public List<List<String>> getAllExpoTokensByIds(List<String> ids) {
        List<ObjectId> objectIds = ids.stream()
                .map(ObjectId::new)
                .toList();
        List<UserEntity> userEntities = userRepository.findByIdIn(objectIds);
        return userEntities.stream()
                .map(UserEntity::getExpoTokens)
                .toList();
    }

}
