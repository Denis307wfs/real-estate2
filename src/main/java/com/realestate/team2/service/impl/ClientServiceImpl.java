package com.realestate.team2.service.impl;

import com.realestate.team2.dto.client.RealEstateTypeDto;
import com.realestate.team2.dto.client.RequestPriceDto;
import com.realestate.team2.dto.client.RequestRoomsDto;
import com.realestate.team2.dto.user.ResponseLoginDto;
import com.realestate.team2.entity.ClientEntity;
import com.realestate.team2.entity.UserEntity;
import com.realestate.team2.enums.PropertyType;
import com.realestate.team2.enums.Rooms;
import com.realestate.team2.enums.State;
import com.realestate.team2.exception.exceptions.ApplicationException;
import com.realestate.team2.model.ClientFilter;
import com.realestate.team2.model.ProjectFilter;
import com.realestate.team2.repository.ClientRepository;
import com.realestate.team2.service.ClientService;
import com.realestate.team2.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final UserService userService;
    private final MongoTemplate mongoTemplate;
    private final ClientRepository clientRepository;

    @Override
    public ClientEntity saveClient(ClientEntity client) {
        return clientRepository.save(client);
    }

    @Override
    public ClientEntity getClientByUserId(String userId) {
        return clientRepository.findByUserId(userId).orElseThrow(() -> new ApplicationException(
                HttpStatus.BAD_REQUEST, "Client with userId %s was not found".formatted(userId)
        ));
    }

    public ResponseLoginDto setRealEstateType(RealEstateTypeDto types) {
        UserEntity userEntity = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ClientEntity client = this.getClientEntityByUserId(userEntity.getId().toString());
        PropertyType[] propertyTypes = Arrays.stream(types.types())
                .map(String::toUpperCase)
                .map(PropertyType::valueOf)
                .toArray(PropertyType[]::new);
        ClientFilter clientFilter = client.getClientFilter();
        clientFilter.setPropertyTypes(propertyTypes);
        client.setClientFilter(clientFilter);
        clientRepository.save(client);
        userService.updateStateById(userEntity.getId(), State.STEP_TWO);
        String state = userService.getStateByUserId(userEntity.getId());

        return new ResponseLoginDto(null, state, userEntity.getRole().toString());
    }

    public ResponseLoginDto setPrice(RequestPriceDto priceDto) {
        UserEntity userEntity = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ClientEntity client = this.getClientEntityByUserId(userEntity.getId().toString());
        ClientFilter clientFilter = client.getClientFilter();
        clientFilter.setMinPrice(priceDto.minPrice());
        clientFilter.setMaxPrice(priceDto.maxPrice());
        client.setClientFilter(clientFilter);
        clientRepository.save(client);
        userService.updateStateById(userEntity.getId(), State.STEP_THREE);
        String state = userService.getStateByUserId(userEntity.getId());

        return new ResponseLoginDto(null, state, userEntity.getRole().toString());
    }

    @Override
    public ResponseLoginDto setRooms(RequestRoomsDto roomsDto) {
        UserEntity userEntity = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ClientEntity client = this.getClientEntityByUserId(userEntity.getId().toString());
        ClientFilter clientFilter = client.getClientFilter();
        if (roomsDto.rooms() != null && !roomsDto.rooms().trim().isEmpty()) {
            clientFilter.setRooms(Rooms.valueOf(roomsDto.rooms().toUpperCase()));
        }
        client.setClientFilter(clientFilter);
        clientRepository.save(client);
        userService.updateStateById(userEntity.getId(), State.DONE);
        String state = userService.getStateByUserId(userEntity.getId());

        return new ResponseLoginDto(null, state, userEntity.getRole().toString());
    }

    @Override
    public List<List<String>> getAllExpoTokensForProjectFilter(ProjectFilter projectFilter) {
        List<String> userIdsInterestedInProject = this.getUserIdsInterestedInProject(projectFilter);
        if (userIdsInterestedInProject.isEmpty()) {
            return Collections.emptyList();
        }
        return userService.getAllExpoTokensByIds(userIdsInterestedInProject);
    }

    @Override
    public List<String> getUserIdsInterestedInProject(ProjectFilter projectFilter) {
        Query query = new Query();
        List<Criteria> criteriaList = new ArrayList<>();
        addFilterToCriteria(projectFilter.getPropertyType(), "clientFilter.propertyTypes", criteriaList);
        addFilterToCriteria(projectFilter.getRooms(), "clientFilter.rooms", criteriaList);
        addPriceToCriteria(projectFilter, criteriaList);
        if (!criteriaList.isEmpty()) {
            query.addCriteria(new Criteria().andOperator(criteriaList.toArray(new Criteria[0])));
        }
        List<ClientEntity> clientEntities = mongoTemplate.find(query, ClientEntity.class);
        return clientEntities.stream()
                .map(ClientEntity::getUserId)
                .toList();
    }

    private void addFilterToCriteria(String projectFilter, String key, List<Criteria> criteriaList) {
        if (projectFilter != null) {
            Criteria criteria = new Criteria().orOperator(
                    Criteria.where(key).is(projectFilter),
                    Criteria.where(key).exists(false)
            );
            criteriaList.add(criteria);
        }
    }

    private void addPriceToCriteria(ProjectFilter projectFilter, List<Criteria> criteriaList) {
        if (projectFilter.getPrice() != null) {
            List<Criteria> priceCriteriaList = new ArrayList<>();
            priceCriteriaList.add(
                    new Criteria().orOperator(
                            Criteria.where("clientFilter.minPrice").exists(false),
                            Criteria.where("clientFilter.minPrice").lte(projectFilter.getPrice())
                    )
            );
            priceCriteriaList.add(
                    new Criteria().orOperator(
                            Criteria.where("clientFilter.maxPrice").exists(false),
                            Criteria.where("clientFilter.maxPrice").gte(projectFilter.getPrice())
                    )
            );
            criteriaList.add(new Criteria().andOperator(priceCriteriaList.toArray(new Criteria[0])));
        }
    }

    private ClientEntity getClientEntityByUserId(String userId) {
        ClientEntity client;
        try {
            client = this.getClientByUserId(userId);
        } catch (ApplicationException ex) {
            log.warn("Client with userId {} was not found", userId);
            client = this.createClient(userId);
        }
        return client;
    }

    private ClientEntity createClient(String userId) {
        ClientEntity client = new ClientEntity();
        ClientFilter filter = new ClientFilter();
        client.setUserId(userId);
        client.setClientFilter(filter);
        return clientRepository.save(client);
    }
}
