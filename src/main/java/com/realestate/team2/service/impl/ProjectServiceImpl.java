package com.realestate.team2.service.impl;

import com.realestate.team2.dto.project.FloorPlanDto;
import com.realestate.team2.dto.project.ProjectDto;
import com.realestate.team2.entity.ProjectEntity;
import com.realestate.team2.entity.UserEntity;
import com.realestate.team2.exception.exceptions.ApplicationException;
import com.realestate.team2.maper.ProjectMapper;
import com.realestate.team2.model.ProjectFilter;
import com.realestate.team2.repository.ProjectRepository;
import com.realestate.team2.service.ProjectService;
import com.realestate.team2.service.S3Uploader;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.data.mongodb.core.query.Update.update;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {

    private final S3Uploader s3Uploader;
    private final MongoTemplate mongoTemplate;
    private final ProjectMapper projectMapper;
    private final ProjectRepository projectRepository;
    private final NotificationServiceImpl notificationService;

    @Override
    public void createProject(ProjectDto dto) {
        this.convertMediaFilesToUrl(dto);
        String ownerId = this.getAuthenticatedUserId();
        ProjectEntity projectEntity = projectMapper.toEntity(dto, ownerId);
        projectEntity.setOwnerId(ownerId);
        projectRepository.save(projectEntity);
        ProjectFilter projectFilter = new ProjectFilter(dto.getGeneralInfo().getPropertyType().toUpperCase(),
                dto.getGeneralInfo().getPrice(), dto.getGeneralInfo().getRooms().toUpperCase());
        notificationService.notifyAllInterestedUsersInProject(projectFilter);
    }

    public boolean isYourProject(String projectId) {
        Optional<ProjectEntity> projectEntityOptional = projectRepository.findById(projectId);

        if (projectEntityOptional.isPresent()) {
            String realOwnerId = projectEntityOptional.get().getOwnerId();
            String brokerId = this.getAuthenticatedUserId();

            if (brokerId.equals(realOwnerId)) {
                return true;
            } else {
                //It is not your project
                throw new ApplicationException(
                        HttpStatus.BAD_REQUEST,
                        "Error! It is not your project."
                );
            }
        } else {
            //Error
            throw new ApplicationException(
                    HttpStatus.BAD_REQUEST,
                    "Error! Can not find project."
            );
        }
    }

    @Override
    public void deleteProject(String projectId) {
        if (this.isYourProject(projectId)) {
            projectRepository.deleteById(projectId);
        }
    }

    @Override
    public void setHideOption(String projectId, Boolean isHidden) {
        if (this.isYourProject(projectId)) {
            mongoTemplate.updateFirst(
                    query(where("id").is(projectId)),
                    update("isHidden", isHidden),
                    ProjectEntity.class
            );
        }
    }

    @Override
    public ProjectDto getProjectById(String projectId) {
        return projectMapper.toDto(this.getProjectEntityById(projectId));
    }

    @Override
    public Page<ProjectDto> getBrokersProjects(Pageable pageable) {
        String ownerId = this.getAuthenticatedUserId();
        return projectRepository.findAllByOwnerId(ownerId, pageable)
                .map(projectMapper::toDto);
    }

    @Override
    public void updateProject(String projectId, ProjectDto dto) {
        this.convertMediaFilesToUrl(dto);
        ProjectEntity projectEntity = this.getProjectEntityById(projectId);
        ProjectEntity updatedProjectEntity = projectMapper.toEntity(dto, projectEntity);
        projectRepository.save(updatedProjectEntity);
    }

    @Override
    public ProjectEntity getProjectEntityById(String projectId) {
        log.info("Get project by id {} method started", projectId);
        return projectRepository.findById(projectId).orElseThrow(() -> {
                    log.warn("Project not found with id: {}", projectId);
                    return new ApplicationException(HttpStatus.BAD_REQUEST,
                            "Project not found with id: %s".formatted(projectId));
                }
        );
    }

    private void convertMediaFilesToUrl(ProjectDto dto) {
        List<Object> photos = dto.getPhotos();
        for (int i = 0; i < photos.size(); i++) {
            Object file = photos.get(i);
            if (file instanceof MultipartFile) {
                String url = s3Uploader.uploadAndReturnUrl((MultipartFile) file);
                photos.remove(i);
                photos.add(i, url);
            }
        }
        if (dto.getDeveloperInformation().getQrCode() instanceof MultipartFile) {
            String url = s3Uploader.uploadAndReturnUrl((MultipartFile) dto.getDeveloperInformation().getQrCode());
            dto.getDeveloperInformation().setQrCode(url);
        }
        if (dto.getDeveloperInformation().getFile() instanceof MultipartFile) {
            String url = s3Uploader.uploadAndReturnUrl((MultipartFile) dto.getDeveloperInformation().getFile());
            dto.getDeveloperInformation().setFile(url);
        }
        for (Map.Entry<String, List<FloorPlanDto>> entry : dto.getFloorPlans().entrySet()) {
            for (FloorPlanDto floorPlan : entry.getValue()) {
                if (floorPlan.getImage() instanceof MultipartFile) {
                    String url = s3Uploader.uploadAndReturnUrl((MultipartFile) floorPlan.getImage());
                    floorPlan.setImage(url);
                }
            }
        }
    }

    private String getAuthenticatedUserId() {
        UserEntity userEntity = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userEntity.getId().toString();
    }

}
