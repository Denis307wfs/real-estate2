package com.realestate.team2.service.impl;

import com.realestate.team2.dto.token.TokenDto;
import com.realestate.team2.dto.user.RequestSaveUserDto;
import com.realestate.team2.dto.user.ResponseLoginDto;
import com.realestate.team2.dto.user.RoleRegisterDto;
import com.realestate.team2.service.RegistrationService;
import com.realestate.team2.service.UserService;
import com.realestate.team2.service.token.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {

    private final UserService userService;

    private final TokenService tokenService;

    @Override
    public void registration(RequestSaveUserDto requestSaveUserDto) {
        userService.saveUserWithPhone(requestSaveUserDto);
    }

    @Override
    public ResponseLoginDto setRole(RoleRegisterDto roleRegisterDto, TokenDto tokenDto) {
        return userService.setRole(roleRegisterDto, tokenDto);
    }
}
