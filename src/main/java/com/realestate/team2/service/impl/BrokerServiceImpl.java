package com.realestate.team2.service.impl;

import com.realestate.team2.dto.broker.RegisterBrokerDto;
import com.realestate.team2.dto.project.ProjectDto;
import com.realestate.team2.dto.user.ResponseLoginDto;
import com.realestate.team2.entity.BrokerEntity;
import com.realestate.team2.entity.UserEntity;
import com.realestate.team2.enums.State;
import com.realestate.team2.exception.exceptions.ApplicationException;
import com.realestate.team2.repository.BrokerRepository;
import com.realestate.team2.service.BrokerService;
import com.realestate.team2.service.ProjectService;
import com.realestate.team2.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class BrokerServiceImpl implements BrokerService {

    private final BrokerRepository brokerRepository;
    private final ProjectService projectService;
    private final UserService userService;

    @Override
    public BrokerEntity saveBroker(BrokerEntity broker) {
        return brokerRepository.save(broker);
    }

    @Override
    public ResponseLoginDto registerBroker(RegisterBrokerDto dto) {
        UserEntity userEntity = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        BrokerEntity broker = getBrokerEntityByUserId(userEntity.getId().toString());
        broker.setName(dto.name());
        broker.setCompanyName(dto.companyName());
        broker.setEmail(dto.email());
        brokerRepository.save(broker);
        userService.updateStateById(userEntity.getId(), State.DONE);
        String state = userService.getStateByUserId(userEntity.getId());

        return new ResponseLoginDto(null, state, userEntity.getRole().toString());
    }

    @Override
    public BrokerEntity getBrokerByUserId(String userId) {
        return brokerRepository.findByUserId(userId).orElseThrow(() -> new ApplicationException(
                HttpStatus.BAD_REQUEST, "Broker with userId %s was not found".formatted(userId)
        ));
    }

    @Override
    public Page<ProjectDto> getAllMyProjects(Pageable pageable) {
        return projectService.getBrokersProjects(pageable);
    }

    @Override
    public void editBrokerData(RegisterBrokerDto dto) {
        String userId = this.getAuthenticatedUserId();
        BrokerEntity broker = this.getBrokerByUserId(userId);
        broker.setName(dto.name());
        broker.setCompanyName(dto.companyName());
        broker.setEmail(dto.email());
        brokerRepository.save(broker);
    }

    private BrokerEntity getBrokerEntityByUserId(String userId) {
        BrokerEntity broker;
        try {
            broker = this.getBrokerByUserId(userId);
        } catch (ApplicationException ex) {
            log.warn("Broker with userId {} was not found", userId);
            broker = this.createBroker(userId);
        }
        return broker;
    }

    private BrokerEntity createBroker(String id) {
        BrokerEntity broker = new BrokerEntity();
        broker.setUserId(id);
        return broker;
    }

    private String getAuthenticatedUserId() {
        UserEntity userEntity = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userEntity.getId().toString();
    }

}
