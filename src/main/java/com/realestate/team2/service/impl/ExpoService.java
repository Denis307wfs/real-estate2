package com.realestate.team2.service.impl;

import io.github.jav.exposerversdk.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ExpoService {

    public void sendPushNotification(String token, String title, String message, Map<String, Object> data) {
        if (!PushClientCustomData.isExponentPushToken(token)) {
            log.error("Token:" + token + " is not a valid token.");
            return;
        }
        ExpoPushMessage expoPushMessage = createExpoPushMessage(token, title, message, data);

        List<ExpoPushMessage> expoPushMessages = new ArrayList<>();
        expoPushMessages.add(expoPushMessage);

        PushClient client;
        try {
            client = new PushClient();
            List<List<ExpoPushMessage>> chunks = client.chunkPushNotifications(expoPushMessages);
            List<CompletableFuture<List<ExpoPushTicket>>> messageRepliesFutures = new ArrayList<>();

            for (List<ExpoPushMessage> chunk : chunks) {
                messageRepliesFutures.add(client.sendPushNotificationsAsync(chunk));
            }
            List<ExpoPushTicket> allTickets = getExpoPushTickets(messageRepliesFutures);
            List<ExpoPushMessageTicketPair<ExpoPushMessage>> zippedMessagesTickets =
                    client.zipMessagesTickets(expoPushMessages, allTickets);

            receiveAndLogAllOkTicketMessages(client, zippedMessagesTickets);
            receiveAndLogAllErrorTicketMessages(client, zippedMessagesTickets);
        } catch (PushClientException e) {
            log.error("A notification was not sent for {} token", token);
        }
    }

    private ExpoPushMessage createExpoPushMessage(
            String token, String title, String message, Map<String, Object> data) {
        ExpoPushMessage expoPushMessage = new ExpoPushMessage();
        expoPushMessage.getTo().add(token);
        expoPushMessage.setTitle(title);
        expoPushMessage.setBody(message);
        expoPushMessage.setData(data);
        return expoPushMessage;
    }

    private List<ExpoPushTicket> getExpoPushTickets(
            List<CompletableFuture<List<ExpoPushTicket>>> messageRepliesFutures) {
        List<ExpoPushTicket> allTickets = new ArrayList<>();
        for (CompletableFuture<List<ExpoPushTicket>> messageReplyFuture : messageRepliesFutures) {
            try {
                allTickets.addAll(messageReplyFuture.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        return allTickets;
    }

    private void receiveAndLogAllOkTicketMessages(
            PushClient client, List<ExpoPushMessageTicketPair<ExpoPushMessage>> zippedMessagesTickets) {
        List<ExpoPushMessageTicketPair<ExpoPushMessage>> okTicketMessages =
                client.filterAllSuccessfulMessages(zippedMessagesTickets);
        String okTicketMessagesString = okTicketMessages.stream()
                .map(p -> "Title: " + p.message.getTitle() + ", Id:" + p.ticket.getId())
                .collect(Collectors.joining(","));
        log.info("Received OK ticket for {} messages: {}", okTicketMessages.size(), okTicketMessagesString);
    }

    private void receiveAndLogAllErrorTicketMessages(
            PushClient client, List<ExpoPushMessageTicketPair<ExpoPushMessage>> zippedMessagesTickets) {
        List<ExpoPushMessageTicketPair<ExpoPushMessage>> errorTicketMessages =
                client.filterAllMessagesWithError(zippedMessagesTickets);
        String errorTicketMessagesString = errorTicketMessages.stream()
                .map(p -> "Title: " + p.message.getTitle() + ", Error: " + p.ticket.getDetails().getError())
                .collect(Collectors.joining(","));
        log.error("Received ERROR ticket for {} messages: {}", errorTicketMessages.size(), errorTicketMessagesString);
    }
}
