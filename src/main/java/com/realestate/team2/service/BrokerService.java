package com.realestate.team2.service;

import com.realestate.team2.dto.broker.RegisterBrokerDto;
import com.realestate.team2.dto.project.ProjectDto;
import com.realestate.team2.dto.user.ResponseLoginDto;
import com.realestate.team2.entity.BrokerEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BrokerService {
    BrokerEntity saveBroker(BrokerEntity broker);

    ResponseLoginDto registerBroker(RegisterBrokerDto dto);

    BrokerEntity getBrokerByUserId(String userId);

    Page<ProjectDto> getAllMyProjects(Pageable pageable);

    void editBrokerData(RegisterBrokerDto dto);
}
