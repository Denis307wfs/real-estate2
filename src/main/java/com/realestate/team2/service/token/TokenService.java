package com.realestate.team2.service.token;

import com.realestate.team2.dto.token.TokenDto;
import jakarta.servlet.http.HttpServletRequest;
import org.bson.types.ObjectId;

public interface TokenService {

    TokenDto getAccessToken(HttpServletRequest request);

    TokenDto getRefreshToken(ObjectId id);

    TokenDto getDisabledAccessToken(String disabledAccessToken);

    TokenDto saveRefreshToken(String refreshToken);

    TokenDto saveDisabledAccessToken(String disabledAccessToken);

    void removeRefreshToken(ObjectId id);
}
