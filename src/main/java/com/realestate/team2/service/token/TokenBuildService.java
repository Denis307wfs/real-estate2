package com.realestate.team2.service.token;

import com.realestate.team2.dto.user.UserDto;
import org.bson.types.ObjectId;

public interface TokenBuildService {

    String generateAccessToken(UserDto userDto, ObjectId refreshTokenId);

    String generateRefreshToken(UserDto userDto);
}
