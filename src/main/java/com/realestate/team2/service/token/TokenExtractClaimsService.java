package com.realestate.team2.service.token;

import org.bson.types.ObjectId;
import org.springframework.security.core.Authentication;

import java.util.Date;

public interface TokenExtractClaimsService {

    String extractPhoneNumber(String token);

    ObjectId extractUserId(String token);

    ObjectId extractRefreshTokenId(String token);

    String extractRole(String token);

    Date extractExpirationDate(String token);

    boolean isTokenValid(String token);

    Authentication getAuthentication(String token);
}
