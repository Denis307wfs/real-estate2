package com.realestate.team2.service;

import com.realestate.team2.dto.filter.DynamicFilterDto;
import com.realestate.team2.dto.project.ProjectDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OfferService {
    
    public Page<ProjectDto> getOffersByUserId(Pageable pageable);

    public Page<ProjectDto> getOffers(Pageable pageable);

    // Task 11. display of offers for me based on initial registration - 4p.
    public Page<ProjectDto> getOffers(String userId, Pageable pageable);

    // Task 12. filtering projects on the back end for the user - 4p.
    public Page<ProjectDto> getOffers(DynamicFilterDto dynamicFilterDto, Pageable pageable);
}
