package com.realestate.team2.service;

import com.realestate.team2.dto.token.TokenDto;
import com.realestate.team2.dto.user.*;
import com.realestate.team2.enums.State;
import org.bson.types.ObjectId;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    UserDto getUserById(ObjectId userId);

    UserDto getUserByPhoneNumber(String phoneNumber);

    String getStateByUserId(ObjectId userId);

    UserDto saveUserWithPhone(RequestSaveUserDto userDto);

    void updateUser(RequestUpdateUserDto userDto);

    void updateStateById(ObjectId id, State newState);

    void deleteUser(ObjectId userId);

    ResponseLoginDto setRole(RoleRegisterDto role, TokenDto tokenDto);

    List<List<String>> getAllExpoTokensByIds(List<String> ids);
}
