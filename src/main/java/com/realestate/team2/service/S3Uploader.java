package com.realestate.team2.service;

import org.springframework.web.multipart.MultipartFile;

public interface S3Uploader {

    String uploadAndReturnUrl(MultipartFile multipartFile);
}
