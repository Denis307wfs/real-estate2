package com.realestate.team2.service;

import com.realestate.team2.dto.client.RealEstateTypeDto;
import com.realestate.team2.dto.client.RequestPriceDto;
import com.realestate.team2.dto.client.RequestRoomsDto;
import com.realestate.team2.dto.user.ResponseLoginDto;
import com.realestate.team2.entity.ClientEntity;
import com.realestate.team2.model.ProjectFilter;

import java.util.List;

public interface ClientService {

    ClientEntity saveClient(ClientEntity client);

    ClientEntity getClientByUserId(String id);

    ResponseLoginDto setRealEstateType(RealEstateTypeDto type);

    ResponseLoginDto setPrice(RequestPriceDto priceDto);

    ResponseLoginDto setRooms(RequestRoomsDto roomsDto);

    List<List<String>> getAllExpoTokensForProjectFilter(ProjectFilter projectFilter);

    List<String> getUserIdsInterestedInProject(ProjectFilter projectFilter);
}
