package com.realestate.team2.maper;

import com.realestate.team2.config.MapperConfig;
import com.realestate.team2.dto.project.GeneralInfoDto;
import com.realestate.team2.model.GeneralInfo;
import org.mapstruct.Mapper;

@Mapper(config = MapperConfig.class)
public interface GeneralInfoMapper {

    GeneralInfo toModel(GeneralInfoDto dto);

    GeneralInfoDto toDto(GeneralInfo model);

}
