package com.realestate.team2.maper;

import com.realestate.team2.config.MapperConfig;
import com.realestate.team2.dto.user.RequestSaveUserDto;
import com.realestate.team2.dto.user.UserDto;
import com.realestate.team2.entity.UserEntity;
import org.mapstruct.Mapper;

@Mapper(config = MapperConfig.class)
public interface UserMapper {

    UserEntity toUserEntity(RequestSaveUserDto dto);

    UserEntity toUserEntity(UserDto userDto);

    UserDto toUserDto(UserEntity user);
}
