package com.realestate.team2.maper;

import com.realestate.team2.config.MapperConfig;
import com.realestate.team2.dto.project.SavedUserProjectDto;
import com.realestate.team2.entity.SavedUserProjectEntity;
import org.mapstruct.Mapper;

@Mapper(config = MapperConfig.class)
public interface SavedUserProjectMapper {

    SavedUserProjectEntity toSavedUserProjectEntity(SavedUserProjectDto savedUserProjectDto);

    SavedUserProjectDto toSavedUserProjectDto(SavedUserProjectEntity savedUserProjectEntity);
}
