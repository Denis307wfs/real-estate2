package com.realestate.team2.maper;

import com.realestate.team2.config.MapperConfig;
import com.realestate.team2.dto.project.FloorPlanDto;
import com.realestate.team2.dto.project.ProjectDto;
import com.realestate.team2.entity.ProjectEntity;
import com.realestate.team2.model.FloorPlan;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.*;

@Mapper(config = MapperConfig.class, uses = {InformationMapper.class, GeneralInfoMapper.class, FloorPlanMapper.class})
public interface ProjectMapper {

    @Mapping(target = "photos", expression = "java(mapPhotos(dto.getPhotos()))")
    @Mapping(target = "floorPlans", expression = "java(toModelFloorPlan(dto.getFloorPlans()))")
    ProjectEntity toEntity(ProjectDto dto, String ownerId);

    ProjectEntity toEntity(ProjectDto dto, @MappingTarget ProjectEntity projectEntity);

    @Mapping(target = "floorPlans", expression = "java(toDtoFloorPlan(entity.getFloorPlans()))")
    ProjectDto toDto(ProjectEntity entity);

    default List<String> mapPhotos(List<Object> photos) {
        if (photos == null || photos.isEmpty()) {
            return Collections.emptyList();
        }
        List<String> photoUrls = new ArrayList<>();
        for (Object photo : photos) {
            photoUrls.add((String) photo);
        }
        return photoUrls;
    }

    default Map<String, List<FloorPlanDto>> toDtoFloorPlan(Map<String, List<FloorPlan>> floorPlans) {
        if (floorPlans == null) {
            return Collections.emptyMap();
        }
        Map<String, List<FloorPlanDto>> floorPlansMapDto = new HashMap<>();
        for (Map.Entry<String, List<FloorPlan>> entry : floorPlans.entrySet()) {
            List<FloorPlan> floorPlanList = entry.getValue();
            List<FloorPlanDto> floorPlanDtoList = new ArrayList<>();
            for (FloorPlan floorPlan : floorPlanList) {
                FloorPlanDto floorPlanDto = toFloorPlanDto(floorPlan);
                floorPlanDtoList.add(floorPlanDto);
            }
            floorPlansMapDto.put(entry.getKey(), floorPlanDtoList);
        }
        return floorPlansMapDto;
    }

    default Map<String, List<FloorPlan>> toModelFloorPlan(Map<String, List<FloorPlanDto>> floorPlansDto) {
        if (floorPlansDto == null) {
            return Collections.emptyMap();
        }
        Map<String, List<FloorPlan>> floorPlans = new HashMap<>();
        for (Map.Entry<String, List<FloorPlanDto>> entry : floorPlansDto.entrySet()) {
            List<FloorPlanDto> floorPlanDtos = entry.getValue();
            List<FloorPlan> floorPlanList = new ArrayList<>();
            for (FloorPlanDto floorPlanDto : floorPlanDtos) {
                FloorPlan floorPlan = toFloorPlan(floorPlanDto);
                floorPlanList.add(floorPlan);
            }
            if (!floorPlansDto.get(entry.getKey()).isEmpty()) {
                floorPlans.put(entry.getKey(), floorPlanList);
            }
        }
        return floorPlans;
    }

    @Mapping(target = "image", expression = "java((String) dto.getImage())")
    FloorPlan toFloorPlan(FloorPlanDto dto);

    FloorPlanDto toFloorPlanDto(FloorPlan model);
}
