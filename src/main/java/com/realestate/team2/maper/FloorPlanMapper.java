package com.realestate.team2.maper;

import com.realestate.team2.config.MapperConfig;
import com.realestate.team2.dto.project.FloorPlanDto;
import com.realestate.team2.model.FloorPlan;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapperConfig.class)
public interface FloorPlanMapper {

    @Mapping(target = "image", expression = "java((String) dto.getImage())")
    FloorPlan toModel(FloorPlanDto dto);

    FloorPlanDto toDto(FloorPlan model);
}
