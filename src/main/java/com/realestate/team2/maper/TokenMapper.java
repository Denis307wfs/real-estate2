package com.realestate.team2.maper;

import com.realestate.team2.config.MapperConfig;
import com.realestate.team2.dto.token.TokenDto;
import com.realestate.team2.entity.DisabledAccessTokenEntity;
import com.realestate.team2.entity.TokenEntity;
import org.mapstruct.*;

@Mapper(config = MapperConfig.class)
public interface TokenMapper {

    @Mapping(target = "refreshToken", expression = "java(tokenDto.getToken())")
    TokenEntity toTokenEntity(TokenDto tokenDto);

    @Mapping(target = "token", expression = "java(tokenEntity.getRefreshToken())")
    TokenDto toTokenDto(TokenEntity tokenEntity);

    @Mapping(target = "token", expression = "java(disabledAccessTokenEntity.getDisabledAccessToken())")
    TokenDto toTokenDto(DisabledAccessTokenEntity disabledAccessTokenEntity);

    @Mapping(target = "disabledAccessToken", expression = "java(tokenDto.getToken())")
    DisabledAccessTokenEntity toDisabledAccessTokenEntity(TokenDto tokenDto);
}
