package com.realestate.team2.maper;

import com.realestate.team2.config.MapperConfig;
import com.realestate.team2.dto.filter.ClientFilterDto;
import com.realestate.team2.enums.PropertyType;
import com.realestate.team2.model.ClientFilter;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.stream.Stream;

@Mapper(config = MapperConfig.class)
public interface ClientFilterMapper {

    ClientFilter toClientFilter(ClientFilterDto dto);

    default PropertyType[] mapToPropertyType(String[] propertyTypes) {
        if (propertyTypes == null) {
            return new PropertyType[0];
        }
        return Stream.of(propertyTypes)
                .map(PropertyType::valueOf)
                .toArray(PropertyType[]::new);
    }

    @Mapping(target = "propertyTypes", expression = "java(mapToString(clientFilter.getPropertyTypes()))")
    ClientFilterDto toDto(ClientFilter clientFilter);

    default String[] mapToString(PropertyType[] propertyTypes) {
        if (propertyTypes == null) {
            return new String[0];
        }
        return Stream.of(propertyTypes)
                .map(Enum::name)
                .toArray(String[]::new);
    }
}
