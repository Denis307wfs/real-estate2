package com.realestate.team2.maper;

import com.realestate.team2.config.MapperConfig;
import com.realestate.team2.dto.project.InformationDto;
import com.realestate.team2.model.Information;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

@Mapper(config = MapperConfig.class)
public interface InformationMapper {

    @Mapping(target = "qrCode", source = "qrCode", qualifiedByName = "mapQrCode")
    @Mapping(target = "file", source = "file", qualifiedByName = "mapFile")
    Information toModel(InformationDto dto);

    @Mapping(target = "qrCode", source = "qrCode", qualifiedByName = "mapQrCode")
    @Mapping(target = "file", source = "file", qualifiedByName = "mapFile")
    Information toModel(InformationDto dto, @MappingTarget Information model);

    InformationDto toDto(Information model);

    @Named("mapQrCode")
    default String mapQrCode(Object qrCode) {
        return qrCode != null ? qrCode.toString() : null;
    }

    @Named("mapFile")
    default String mapFile(Object file) {
        return file != null ? file.toString() : null;
    }
}
