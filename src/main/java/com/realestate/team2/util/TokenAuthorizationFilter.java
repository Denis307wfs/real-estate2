package com.realestate.team2.util;

import com.realestate.team2.dto.token.TokenDto;
import com.realestate.team2.service.token.TokenExtractClaimsService;
import com.realestate.team2.service.token.TokenService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Slf4j
@Component
@RequiredArgsConstructor
public class TokenAuthorizationFilter extends OncePerRequestFilter
        implements AccessDeniedHandler, AuthenticationEntryPoint {

    private final TokenService tokenService;

    private final TokenExtractClaimsService tokenExtractClaimsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (authHeader != null && !authHeader.isBlank() && authHeader.startsWith("Bearer")) {
            TokenDto tokenDto = tokenService.getAccessToken(request);
            if (tokenService.getDisabledAccessToken(tokenDto.getToken()) == null
                    && tokenExtractClaimsService.isTokenValid(tokenDto.getToken())) {
                Authentication authentication = tokenExtractClaimsService.getAuthentication(tokenDto.getToken());
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void handle(HttpServletRequest request,
                       HttpServletResponse response,
                       AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.getWriter().print("Access denied");
        response.setStatus(HttpStatus.FORBIDDEN.value());
    }

    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException, ServletException {
        response.getWriter().print("Access token isn't valid or expired");
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
    }

}
