package com.realestate.team2.entity;

import com.realestate.team2.model.ClientFilter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Document(collection = "client")
public class ClientEntity extends BaseEntity {

    private String userId;

    private ClientFilter clientFilter;

}
