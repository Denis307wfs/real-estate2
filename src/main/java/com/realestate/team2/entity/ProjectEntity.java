package com.realestate.team2.entity;

import com.realestate.team2.model.FloorPlan;
import com.realestate.team2.model.GeneralInfo;
import com.realestate.team2.model.Information;
import com.realestate.team2.model.PaymentPlan;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Document(collection = "project")
public class ProjectEntity extends BaseEntity {

    private String ownerId;

    private GeneralInfo generalInfo;

    private List<String> photos;

    private Set<String> facilities;

    private Map<String, List<FloorPlan>> floorPlans;

    private List<PaymentPlan> paymentPlans;

    private Boolean fixedPrice;

    private Information developerInformation;

    private Boolean isTop;

    private Boolean isHidden;

    private Boolean isNotify;

}
