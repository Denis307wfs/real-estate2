package com.realestate.team2.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Document(collection = "broker")
public class BrokerEntity extends BaseEntity {

    @Indexed(unique = true)
    private String name;

    @Indexed(unique = true)
    private String companyName;

    @Indexed(unique = true)
    private String email;

    private String userId;
}
