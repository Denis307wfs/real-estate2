package com.realestate.team2.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Document(collection = "saved_user_project")
public class SavedUserProjectEntity extends BaseEntity {

    private ObjectId userId;

    private List<ObjectId> savedProjectIds;
}
