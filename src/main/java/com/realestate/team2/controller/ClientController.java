package com.realestate.team2.controller;

import com.realestate.team2.dto.client.RealEstateTypeDto;
import com.realestate.team2.dto.client.RequestPriceDto;
import com.realestate.team2.dto.client.RequestRoomsDto;
import com.realestate.team2.dto.filter.DynamicFilterDto;
import com.realestate.team2.dto.project.ProjectDto;
import com.realestate.team2.dto.user.ResponseLoginDto;
import com.realestate.team2.service.ClientService;
import com.realestate.team2.service.OfferService;
import com.realestate.team2.service.SavedUserProjectService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@PreAuthorize("hasRole('CLIENT')")
@RequestMapping("api/v1/client")
public class ClientController {

    private final OfferService offerService;

    private final ClientService clientService;

    private final SavedUserProjectService savedUserProjectService;

    @PostMapping("/estate")
    public ResponseEntity<ResponseLoginDto> registerRealEstateType(@RequestBody RealEstateTypeDto estate) {
        return ResponseEntity.ok(clientService.setRealEstateType(estate));
    }

    @PostMapping("/price")
    public ResponseEntity<ResponseLoginDto> registerPrice(@RequestBody RequestPriceDto priceDto) {
        return ResponseEntity.ok(clientService.setPrice(priceDto));
    }

    @PostMapping("/rooms")
    public ResponseEntity<ResponseLoginDto> registerRooms(@RequestBody RequestRoomsDto roomsDto) {
        return ResponseEntity.ok(clientService.setRooms(roomsDto));
    }

    @GetMapping("/for_you")
    public ResponseEntity<Page<ProjectDto>> getOffersByClientFilter(Pageable pageable) {

        return ResponseEntity.ok(
                offerService.getOffersByUserId(pageable)
        );
    }

    @GetMapping("/offers")
    public ResponseEntity<Page<ProjectDto>> getOffersByDynamicFilter(
            @Valid @RequestBody DynamicFilterDto dynamicFilterDto,
            Pageable pageable
    ) {

        return ResponseEntity.ok(
                offerService.getOffers(dynamicFilterDto, pageable)
        );
    }

    @GetMapping("/all_offers")
    public ResponseEntity<Page<ProjectDto>> getAllOffers(
            Pageable pageable
    ) {

        return ResponseEntity.ok(
                offerService.getOffers(pageable)
        );
    }

    @GetMapping("/get_saved_projects")
    public ResponseEntity<?> getSavedProjects() {
        return ResponseEntity.ok(savedUserProjectService.getSavedUserProjects());
    }

    @PostMapping("/save_saved_project")
    public ResponseEntity<?> saveSavedProjects(@RequestParam ObjectId projectId) {
        savedUserProjectService.addToSavedUserProjects(projectId);
        return ResponseEntity.ok("Save project to saved");
    }

    @DeleteMapping("/remove_saved_project")
    public ResponseEntity<?> deleteSavedProjects(@RequestParam ObjectId projectId) {
        savedUserProjectService.removeFromSavedUserProjects(projectId);
        return ResponseEntity.ok("Delete project to saved");
    }
}
