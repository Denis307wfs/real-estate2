package com.realestate.team2.controller;

import com.realestate.team2.dto.user.RequestSaveUserDto;
import com.realestate.team2.dto.user.ResponseLoginDto;
import com.realestate.team2.dto.user.RoleRegisterDto;
import com.realestate.team2.enums.Role;
import com.realestate.team2.service.RegistrationService;
import com.realestate.team2.service.token.TokenService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/registration")
@RequiredArgsConstructor
public class RegistrationController {

    private final RegistrationService registrationService;

    private final TokenService tokenService;

    @PostMapping
    public ResponseEntity<Void> registration(@RequestBody @Valid RequestSaveUserDto requestSaveUserDto) {
        // otpService
        registrationService.registration(requestSaveUserDto);
        return ResponseEntity.ok().build();
    }

    @RolesAllowed(Role.Constants.USER_VALUE)
    @PostMapping("/role")
    public ResponseEntity<ResponseLoginDto> registerRole(@RequestBody @Valid RoleRegisterDto roleRegisterDto,
                                                         HttpServletRequest httpServletRequest) {
        return ResponseEntity.ok(registrationService.setRole(
                roleRegisterDto, tokenService.getAccessToken(httpServletRequest)));
    }

}
