package com.realestate.team2.controller;

import com.realestate.team2.dto.project.ProjectDto;
import com.realestate.team2.service.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@PreAuthorize("hasRole('BROKER')")
@RequestMapping("api/v1/projects")
public class ProjectController {

    private final ProjectService projectService;

    @PostMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Void> createProject(@ModelAttribute ProjectDto projectDto) {
        projectService.createProject(projectDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/id/{projectId}")
    public ResponseEntity<ProjectDto> getProjectById(@PathVariable String projectId) {
        return ResponseEntity.ok(projectService.getProjectById(projectId));
    }

    @PutMapping("/{projectId}/update")
    public ResponseEntity<Void> updateProjectById(@PathVariable String projectId,
                                                  @ModelAttribute ProjectDto projectDto) {
        projectService.updateProject(projectId, projectDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{projectId}/delete")
    public ResponseEntity<Void> deleteProjectById(@PathVariable String projectId) {
        projectService.deleteProject(projectId);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{projectId}/hide")
    public ResponseEntity<Void> setHideOption(@PathVariable String projectId) {
        projectService.setHideOption(projectId,true);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{projectId}/show")
    public ResponseEntity<Void> setShowOption(@PathVariable String projectId) {
        projectService.setHideOption(projectId,false);
        return ResponseEntity.ok().build();
    }
}
