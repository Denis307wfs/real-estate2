package com.realestate.team2.controller;

import com.realestate.team2.dto.user.RequestLoginDto;
import com.realestate.team2.dto.user.ResponseLoginDto;
import com.realestate.team2.service.AuthenticationService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/authentication")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping
    public ResponseEntity<ResponseLoginDto> login(@RequestBody RequestLoginDto requestLoginDto) {
        // otpService
        return ResponseEntity.ok(authenticationService.login(requestLoginDto));
    }

    @PostMapping("/refresh")
    public ResponseEntity<ResponseLoginDto> refresh(HttpServletRequest httpServletRequest) {
        return ResponseEntity.ok(authenticationService.refreshToken(httpServletRequest));
    }

}
