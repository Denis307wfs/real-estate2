package com.realestate.team2.controller;

import com.realestate.team2.dto.broker.RegisterBrokerDto;
import com.realestate.team2.dto.project.ProjectDto;
import com.realestate.team2.dto.user.ResponseLoginDto;
import com.realestate.team2.service.BrokerService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@PreAuthorize("hasRole('BROKER')")
@RequestMapping("api/v1/broker")
public class BrokerController {

    private final BrokerService brokerService;

    @PostMapping("/register")
    public ResponseEntity<ResponseLoginDto> registerBroker(@RequestBody @Valid RegisterBrokerDto dto) {
        return ResponseEntity.ok(brokerService.registerBroker(dto));
    }

    @GetMapping("/myProjects")
    public ResponseEntity<Page<ProjectDto>> getAllMyProjects(Pageable pageable) {
        return ResponseEntity.ok(brokerService.getAllMyProjects(pageable));
    }

    @PutMapping("/edit")
    public ResponseEntity<Void> editBrokerData(@RequestBody @Valid RegisterBrokerDto dto) {
        brokerService.editBrokerData(dto);
        return ResponseEntity.ok().build();
    }
}
