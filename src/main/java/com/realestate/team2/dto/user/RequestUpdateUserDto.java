package com.realestate.team2.dto.user;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import org.bson.types.ObjectId;

@JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy.class)
public record RequestUpdateUserDto(ObjectId id, String role) {
}
