package com.realestate.team2.dto.user;

import com.realestate.team2.dto.BaseDto;
import com.realestate.team2.enums.Role;
import com.realestate.team2.enums.State;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class UserDto extends BaseDto implements Serializable {

    private String phoneNumber;

    private Role role;

    private State state;

    private Boolean enabled;

    private Boolean locked;

}
