package com.realestate.team2.dto.user;

public record RoleRegisterDto(String role) {
}
