package com.realestate.team2.dto.broker;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.Email;

@JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy.class)
public record RegisterBrokerDto(
        String name,

        String companyName,

        @Email
        String email
) {
}
