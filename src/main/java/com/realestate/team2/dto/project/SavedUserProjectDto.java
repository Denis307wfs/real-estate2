package com.realestate.team2.dto.project;

import lombok.Data;
import org.bson.types.ObjectId;

import java.util.List;

@Data
public class SavedUserProjectDto {

    private ObjectId userId;

    private List<ObjectId> savedProjectIds;
}
