package com.realestate.team2.dto.project;

import com.realestate.team2.model.Location;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Data;

@Data
public class GeneralInfoDto {
    @NotEmpty
    @NotNull
    private String name;

    private String area;

    private String propertyType;

    private String rooms;

    private Double size;

    @PositiveOrZero
    private Integer price;

    @PositiveOrZero
    private Double priceForFt;

    private Location location;

    private String developerName;

    private String completionStatus;

    private String about;

}
