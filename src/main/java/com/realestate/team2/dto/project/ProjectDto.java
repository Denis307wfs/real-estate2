package com.realestate.team2.dto.project;

import com.realestate.team2.model.PaymentPlan;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
@AllArgsConstructor
public class ProjectDto {
    private GeneralInfoDto generalInfo;
    private List<Object> photos;
    private Set<String> facilities;
    private Map<String, List<FloorPlanDto>> floorPlans = new HashMap<>();
    private List<PaymentPlan> paymentPlans;
    private Boolean fixedPrice;
    private InformationDto developerInformation;
    private Boolean isTop;
    private Boolean isHidden;
    private Boolean isNotify;

    public ProjectDto() {
        this.initializeFloorPlans();
    }

    public void initializeFloorPlans() {
        String[] keys = {"STUDIO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT"};
        for (String key : keys) {
            floorPlans.computeIfAbsent(key, k -> new ArrayList<>());
        }
    }
}
