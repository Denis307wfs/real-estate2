package com.realestate.team2.dto.project;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class InformationDto {

    private Object qrCode;

    private String trakheesiNumber;

    private LocalDateTime dateOfExpiration;

    private Object file;
}
