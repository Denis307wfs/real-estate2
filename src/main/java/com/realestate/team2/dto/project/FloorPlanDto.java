package com.realestate.team2.dto.project;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FloorPlanDto {

    private Object image;

    private Double price;

    private Double size;

}
