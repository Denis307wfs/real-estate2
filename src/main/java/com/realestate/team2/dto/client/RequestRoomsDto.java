package com.realestate.team2.dto.client;

public record RequestRoomsDto(String rooms) {
}
