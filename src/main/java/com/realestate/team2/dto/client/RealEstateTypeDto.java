package com.realestate.team2.dto.client;

public record RealEstateTypeDto(String[] types) {
}
