package com.realestate.team2.dto.filter;

import jakarta.validation.constraints.PositiveOrZero;

import java.util.List;

public record DynamicFilterDto(
        List<String> area,
        List<String> propertyTypes,
        @PositiveOrZero
        Integer minSize,
        @PositiveOrZero
        Integer maxSize,
        String rooms,
        @PositiveOrZero
        Integer minPrice,
        @PositiveOrZero
        Integer maxPrice
) {
}
