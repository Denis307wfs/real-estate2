package com.realestate.team2.dto.filter;

public record ClientFilterDto(
        String[] propertyTypes,
        Integer minPrice,
        Integer maxPrice,
        String rooms) {
}
