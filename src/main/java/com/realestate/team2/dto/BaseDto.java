package com.realestate.team2.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@SuperBuilder
public class BaseDto implements Serializable {

    protected ObjectId id;

    protected LocalDateTime createdAt;

    protected LocalDateTime updatedAt;
}
