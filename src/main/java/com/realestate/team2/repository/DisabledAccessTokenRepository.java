package com.realestate.team2.repository;

import com.realestate.team2.entity.DisabledAccessTokenEntity;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DisabledAccessTokenRepository extends MongoRepository<DisabledAccessTokenEntity, ObjectId> {

    Optional<DisabledAccessTokenEntity> findByDisabledAccessToken(String disabledAccessToken);

}
