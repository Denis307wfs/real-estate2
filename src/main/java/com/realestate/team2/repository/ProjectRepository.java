package com.realestate.team2.repository;

import com.realestate.team2.dto.project.ProjectDto;
import com.realestate.team2.entity.ProjectEntity;
import com.realestate.team2.enums.PropertyType;
import com.realestate.team2.enums.Rooms;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReadPreference;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProjectRepository extends MongoRepository<ProjectEntity, ObjectId> {

    Optional<ProjectEntity> findById(String id);

    Page<ProjectEntity> findAllByOwnerId(String ownerId, Pageable pageable);

    void deleteById(String projectId);


    //Error when I try to use findAll() with DTO
    @Query(value = "{}")
    Page<ProjectDto> findAllCustom(Pageable pageable);

    /*
        db.collection.find().sort( { $natural: 1 } )
        db.posts.find({
            realEstateType: 'realEstateType',
            price: {$lte: value, $gte: value},
            bedrooms: 'bedrooms',
            bathrooms: 'bathrooms'
        })

        @Query(sort = "{$natural:-1}", value= ...)
        */
    @Query(
            value = "{"
                    + " 'generalInfo.propertyType' : {$in : ?0},"
                    + " 'generalInfo.price'        : {$gte : ?1, $lte : ?2},"
                    + " 'generalInfo.rooms'        : ?3,"
                    + "}"
    )
    @ReadPreference
    Page<ProjectDto> findByClientFilter(
            Pageable pageable,
            PropertyType[] propertyTypes,
            int minPrice,
            int maxPrice,
            Rooms rooms
    );

}
