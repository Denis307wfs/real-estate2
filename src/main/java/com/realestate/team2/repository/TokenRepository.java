package com.realestate.team2.repository;

import com.realestate.team2.entity.TokenEntity;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends MongoRepository<TokenEntity, ObjectId> {
}
