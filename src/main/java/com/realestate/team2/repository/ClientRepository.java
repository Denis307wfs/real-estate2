package com.realestate.team2.repository;

import com.realestate.team2.entity.ClientEntity;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends MongoRepository<ClientEntity, ObjectId> {

    Optional<ClientEntity> findByUserId(String userId);
}
