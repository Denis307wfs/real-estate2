package com.realestate.team2.repository;

import com.realestate.team2.entity.UserEntity;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<UserEntity, ObjectId> {

    Optional<UserEntity> findByPhoneNumber(String phoneNumber);

    @Query(value = "{ '_id' : ?0 }", fields = "{ 'state' : 1 }")
    Optional<Document> findStateByUserId(ObjectId userId);

    List<UserEntity> findByIdIn(List<ObjectId> ids);
}
