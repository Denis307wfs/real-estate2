package com.realestate.team2.repository;

import com.realestate.team2.entity.SavedUserProjectEntity;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SavedUserProjectRepository extends MongoRepository<SavedUserProjectEntity, ObjectId> {

    SavedUserProjectEntity findSavedUserProjectEntityByUserId(ObjectId userId);
}
