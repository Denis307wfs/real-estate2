package com.realestate.team2.repository;

import com.realestate.team2.entity.BrokerEntity;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface BrokerRepository extends MongoRepository<BrokerEntity, ObjectId> {
    Optional<BrokerEntity> findByUserId(String userId);
}
