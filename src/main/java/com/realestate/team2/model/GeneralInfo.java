package com.realestate.team2.model;

import com.realestate.team2.enums.Area;
import com.realestate.team2.enums.CompletionStatus;
import com.realestate.team2.enums.PropertyType;
import com.realestate.team2.enums.Rooms;
import lombok.Data;

@Data
public class GeneralInfo {

    private String name;

    private Area area;

    private PropertyType propertyType;

    private Rooms rooms;

    private Double size;

    private Integer price;

    private Double priceForFt;

    private Location location;

    private String developerName;

    private CompletionStatus completionStatus;

    private String about;
}
