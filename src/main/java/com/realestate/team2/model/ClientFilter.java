package com.realestate.team2.model;

import com.realestate.team2.enums.PropertyType;
import com.realestate.team2.enums.Rooms;
import lombok.Data;

@Data
public class ClientFilter {

    private PropertyType[] propertyTypes;

    private Integer minPrice;

    private Integer maxPrice;

    private Rooms rooms;
}
