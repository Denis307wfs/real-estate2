package com.realestate.team2.model;

import lombok.Data;

@Data
public class PaymentPlan {

    private String name;

    private Integer percent;

}
