package com.realestate.team2.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProjectFilter {
    private String propertyType;
    private Integer price;
    private String rooms;
}
