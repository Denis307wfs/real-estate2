package com.realestate.team2.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Information {

    private String qrCode;

    private String trakheesiNumber;

    private LocalDateTime dateOfExpiration;

    private String file;

}
