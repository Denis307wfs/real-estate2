package com.realestate.team2.model;

import lombok.Data;

@Data
public class FloorPlan {

    private String image;

    private Double price;

    private Double size;

}
