package com.realestate.team2.model;

import lombok.Data;

@Data
public class Location {

    private Integer latitude;

    private Integer longitude;
}
